# TODO

#### A few words on semantic versioning and pull request
- When a fix is solved the third digit is increased

### Fixes ( remember to add tag after fixing, increase last digit )
1. Verify required php modules against WordPress and PHP Composer: search hints: composer php modules requirements, wordpress php modules requirements.
2. Install netstat like service.
3. Insert mysql_secure_installations queries in provision.sh.
4. Fix SELinux policies.

### Evolutions ( version 0.2 )
1. Include composer installation in provision.sh.
2. Add packagist requirements.
3. Install npm.

### Evolutions ( version 1.0 )
1. Deploy on remote server service.

### Evolutions ( version 1.1 )
2. Provisioning script Debian.
3. Evolve to nvm.

### Current impediments
- SSH policies of Infomaniak (ask Jerome, or team).
